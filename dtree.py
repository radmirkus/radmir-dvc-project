import numpy as np
import pandas as pd
from sklearn import tree

def int_to_name(num: int):
    return ["Iris-versicolor", "Iris-setosa", "Iris-virginica"][num]

def name_to_int(name: str):
    return {
      "Iris-versicolor": 0,
      "Iris-setosa": 1,
      "Iris-virginica": 2
      }[name]

if __name__ == "__main__":
  p = pd.read_csv('iris.data', names=["sepal_length", "sepal_width",
   "petal_length", "petal_width", "class"])

  # kmeans = KMeans(n_clusters=3, random_state=0, algorithm="lloyd", n_init=100)
  clf = tree.DecisionTreeClassifier()

  X = p[["sepal_length", "sepal_width", "petal_length", "petal_width"]]
  Y = list(map(name_to_int, p["class"]))
  # kmeans.fit(X)


  clf = clf.fit(X, Y)

  a = [int_to_name(x) for x in clf.predict(X)]  # predicted
  b = p["class"].values.tolist()  # from data

  oks = list(map(lambda x: x[0]==x[1], zip(a,b)))
  my_metric = oks.count(True) / len(oks)
  print(my_metric)

  p["class"] = a
  p.to_csv('data/predict.txt', index=False)

  with open("data/eval.txt", 'w') as fw:
    fw.write(str(my_metric))